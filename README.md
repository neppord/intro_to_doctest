Introduction to Doctest
=======================

This repository is a introduction to how doctest works. It includes a finished
product so if you are interested in looking into how I (Samuel Ytterbrink)
solved the problem, just look at the history.

The last commit will remove all the code and only leave the documentation and 
the tests.

Your assignment is to fulfill those tests and implement the bulk of the
application

Running the tests
-----------------

To run the tests type on the comand line

```sh
python -m doctset <file>
```

where `<file>`is the file you are working on.

The `-m` flag tells python to run the module, in this case doctest. This is very
helpfull if you have a module that can run as a script.
