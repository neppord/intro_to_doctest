EMPTY_BOARD = "1|2|3\n-----\n4|5|6\n-----\n7|8|9"
PLAYER_MARKER = "X"
AI_MARKER = "O"
EMPTY_DB = set()


def is_legal_move(board, move):
    """
    Returns true if the board accepts the move.
    Example
    >>> is_legal_move(EMPTY_BOARD, "a")
    False
    >>> is_legal_move(EMPTY_BOARD, "1")
    True
    >>> is_legal_move(EMPTY_BOARD.replace("1", "X"), "1")
    False
    """


def make_move(board, cell, marker):
    r"""
    Creates a new board from the old replacing cell with marker
    Example:
    >>> make_move(EMPTY_BOARD, "1", "X")
    'X|2|3\n-----\n4|5|6\n-----\n7|8|9'
    >>> make_move(EMPTY_BOARD, "2", "X")
    '1|X|3\n-----\n4|5|6\n-----\n7|8|9'
    """


def legal_moves(board):
    """
    Returns all legal cells from board
    Example:
    >>> sorted(legal_moves(EMPTY_BOARD))
    ['1', '2', '3', '4', '5', '6', '7', '8', '9']
    >>> sorted(legal_moves(make_move(EMPTY_BOARD, "1", "X")))
    ['2', '3', '4', '5', '6', '7', '8', '9']
    """


def ai_want_to_give_up(db, text_board, moves, marker, enemy_marker):
    r"""
    Uses the database and the avaliable moves to ficure out if it already lost
    Example:
    >>> ai_want_to_give_up(EMPTY_DB, EMPTY_BOARD, [], "O", "X")
    True
    >>> ai_want_to_give_up(
    ...  EMPTY_DB,
    ...  "X|X|3\n-----\n4|5|6\n-----\n7|8|9",
    ...  ["3", "4", "5", "6", "7", "8", "9"],
    ...  "O",
    ...  "X")
    ...
    False
    >>> ai_want_to_give_up(
    ...  EMPTY_DB,
    ...  "X|X|X\n-----\n4|5|6\n-----\n7|8|9",
    ...  ["4", "5", "6", "7", "8", "9"],
    ...  "O",
    ...  "X")
    ...
    True
    >>> ai_want_to_give_up(
    ...  EMPTY_DB,
    ...  "X|2|3\n-----\nX|5|6\n-----\nX|8|9",
    ...  ["2", "3", "5", "6", "8", "9"],
    ...  "O",
    ...  "X")
    ...
    True
    >>> ai_want_to_give_up(
    ...  EMPTY_DB,
    ...  "X|2|3\n-----\n4|X|6\n-----\n7|8|X",
    ...  ["2", "3", "4", "6", "7", "8"],
    ...  "O",
    ...  "X")
    ...
    True
    >>> ai_want_to_give_up(
    ...  EMPTY_DB,
    ...  "1|2|X\n-----\n4|X|6\n-----\nX|8|9",
    ...  ["1", "2", "4", "6", "8", "9"],
    ...  "O",
    ...  "X")
    ...
    True
    >>> ai_want_to_give_up(
    ...  set([
    ...    "O|2|3\n-----\n4|5|6\n-----\n7|8|9",
    ...  ]),
    ...  "1|2|3\n-----\n4|5|6\n-----\n7|8|9",
    ...  ["1"],
    ...  "O",
    ...  "X")
    ...
    True
    """


def ai_none_losing_moves(db, board, legal_moves, marker):
    r"""
    Returns moves that will not leed to a already known losing board.
    Example:
    >>> ai_none_losing_moves(EMPTY_DB, EMPTY_BOARD, ["1"], "X")
    ['1']
    >>> ai_none_losing_moves(
    ...  {"X|2|3\n-----\n4|5|6\n-----\n7|8|9"},
    ...  EMPTY_BOARD, ["1"], "X")
    ...
    []
    """


def has_player_won(text_board, marker):
    r"""
    Exampels:
    >>> has_player_won("X|X|X\n-----\n4|5|6\n-----\n7|8|9", "X")
    True
    >>> has_player_won("X|X|X\n-----\n4|5|6\n-----\n7|8|9", "O")
    False
    >>> has_player_won("O|2|3\n-----\nO|5|6\n-----\nO|8|9", "O")
    True
    >>> has_player_won("O|2|3\n-----\n4|O|6\n-----\n7|8|O", "O")
    True
    >>> has_player_won("1|2|O\n-----\n4|O|6\n-----\nO|8|9", "O")
    True
    """


def main():
    import random
    board = EMPTY_BOARD
    choice = ""
    ai_db = EMPTY_DB
    old_board = EMPTY_BOARD
    while choice != "q":
        print board
        choice = raw_input("Type a number to choose a move(q to quit): ")
        if is_legal_move(board, choice):
            board = make_move(board, choice, PLAYER_MARKER)
        else:
            print "Not a legal move, try again"
            continue
        ai_moves = legal_moves(board)
        ai_give_up = ai_want_to_give_up(
            ai_db,
            board,
            ai_moves,
            AI_MARKER,
            PLAYER_MARKER
        )
        if ai_give_up:
            print "The AI gives up You win!"
            ai_db.add(old_board)
            board = EMPTY_BOARD
            old_board = EMPTY_BOARD
        else:
            ai_moves = ai_none_losing_moves(ai_db, board, ai_moves, AI_MARKER)
            ai_move = random.choice(ai_moves)
            board = make_move(board, ai_move, AI_MARKER)
            old_board = board
        if has_player_won(board, AI_MARKER):
            print board
            print "The AI won!"
            board = EMPTY_BOARD
            old_board = EMPTY_BOARD


if __name__ == "__main__":
    main()
